#!/usr/bin/env python3
from bs4 import BeautifulSoup
import requests
import re
import json
import time

#urllib for normal webrequest without nextevent
#header = {'User-Agent' : 'Kufa API Bot v1.3.1-Stable'} #User Agent is set with some extra info.
#req = urllib.request.Request("https://www.kufa.ch/?page_1=2", headers=header)
#with urllib.request.urlopen(req) as url:
#    s = url.read()
#    s = str(s)
#end of urllib request

request = requests.get("https://www.kufa.ch/?page_1=2")
s = request.text

def getLinks():
    soup = BeautifulSoup(s,"html.parser")
    linkArray = []
    events = soup.find_all("div", { "class" : "post-listing-entry" }) #Find every div with class Progentry and save it to variable entries
    for event in events:
        try:
            eventLink = event.find("a", href=True)['href'] #Trying to get Facebook Event Links
            linkArray.append(eventLink)
        except:
            pass # If no Link is present, facebookEvent ist Null
        #print(entry)
    return linkArray

def processLinks(linkArray):
    # Make a switch in case there is no main act!
    hasNoMainAct = False
    jsonData = {"events": []}
    isSpecial = False #Set to False because it is only used by Special entries (Default)
    plus18 = False #Set plus18 to False (Default)
    plus20 = False #Set plus20 to False (Default)
    plus21 = False #Set plus21 to False (Default)
    plus25 = False #Set plus25 to False (Default)
    plus30 = False #Set plus30 to False (Default)
    isTipp = False #Set isTipp to False (Default)
    isNew = False #Set  isNew to False (Default)
    isSoldOut = False #Set isSoldOut to False (Default)
    isExclusive = False #Set isExclusive to False (Default)
    till0530 = False #Set till0530 to False (Default)
    isFree = False #Set isFree to False (Default)
    freeMember = False #Set freeMember to False (Default)
    isCancelled = False #Set isCancelled to False (Default)
    isMoved = False # Set isMoved to False (Default)
    isGoes = False # Set isGoes to False (Default)
    facebookEvent = None
    for link in linkArray:
        #header = {'User-Agent' : 'Kufa API Bot v1.3.1-Stable'} #User Agent is set with some extra info.
        #req = urllib.request.Request(link, headers=header)
        #with urllib.request.urlopen(req) as url:
        #    s = url.read()
        request = requests.get(link)
        s = request.text
        soup = BeautifulSoup(s,"html.parser")
        header = soup.find("h1", { "class" : "page-header" })
        eventHeader = soup.find("title").get_text().replace(" - Kulturfabrik KUFA Lyss", "")
        header_small = soup.find("h1", { "class" : "page-header" }).find("small").extract()
        date_weekday = soup.find("time").find("small").get_text()
        date_extract = soup.find("time").find("small").extract()
        date = soup.find("time").get_text()
        eventImage = soup.find("div", {"class": ["col-sm-8", "col-md-7", "col-lg-8"]})
        eventImg = [x["data-src"] for x in eventImage.findAll("img", {"class": ["attachment-large", "size-large", "wp-post-image"]})] #Get all Act Images
        doors = [x.get_text() for x in soup.find("dl", {"class": ["times", "dl-horizontal"]}).findAll("dd")]
        try:
            facebookEvent = soup.find("a", {"class" : ["label","label-facebook"]})["href"]
        except:
            pass
        eventDescription = [x.get_text() for x in soup.find("div", {"class" : ["entry-content","indent"]}).findAll("p")]
        eventDescription = "\n".join(eventDescription)
        if eventDescription == "":
            try:
                eventDescription = soup.find("div", {"class": ["entry-content", "indent"]}).get_text()
            except:
                pass

        mainActs = [] # Initialize array
        acts = soup.findAll("li", {"class": "main-act"})
        if len(acts) == 1:
            # Special 1 Act events with some weird headers. need to account for those
            

            # Only one main act so we must parse differently
            ##mainActDescription = acts[0].find("div", {"class":"indent"})#
            try: 
                mainActDescription = soup.find("li",{"class", "main-act"}).find("div", {"class", "indent"}).get_text()
            except:
                mainActDescription = soup.find("main",{"class", "main"}).find("div",{"class", "indent"}).get_text()
            ##mainActDescription = "DEBUG"
            mainActImage = None
            try:
                mainActGenre = soup.find("small", {"class": "category"}).get_text()
                soup.find("small", {"class": "category"}).extract()
            except:
                mainActGenre = None
            try:
                soup.find("em").extract()
            except:
                pass
            mainActName = soup.find("span", {"class":"main-act"}).find("a").get_text().replace("  ", "")
            try:
                if soup.find("span", {"class":"main-act"}).find("a")["href"].startswith("#") == True:
                    print("Startwith")
                    mainActWebsite = soup.find("h3").find("a")["href"]
                else:    
                    mainActWebsite = soup.find("span", {"class":"main-act"}).find("a")["href"]
            except:
                mainActWebsite = None
            try:
                if soup.find("span", {"class":"main-act"}).find("a")["href"].startswith("#") == True:
                    mainActListen = soup.find("small",{"class": "listen"}).find("a")["href"]
                else:    
                    mainActListen = soup.find("span", {"class":"main-act"}).find("small", {"class": None}).find("a")["href"]
            except:
                mainActListen = None

            print(mainActName)
            mainActs.append({"description": mainActDescription,
                "genre": mainActGenre,
                "name": mainActName,
                "website": mainActWebsite,
                "image": mainActImage,
                "listen": mainActListen}) # Add listen url later

        else:
            # If we have multiple acts we parse like this
            for act in acts:
                mainActDescription = [x.get_text() for x in act.findAll("p")]
                mainActDescription = "".join(mainActDescription)
                # Check if mainActDescription is empty then check other html elements
                try:
                    mainActImage = act.find("img", {"class": ["attachment-large", "size-large"]})["data-src"]
                except:
                    mainActImage = None
                try:
                    mainActGenre = act.find("small", {"class": "category"}).get_text()
                    act.find("small", {"class": "category"}).extract()
                except:
                    mainActGenre = None
                try:
                    mainActListen = act.find("small",{"class": "listen"}).find("a")["href"]
                    act.find("small",{"class": "listen"}).extract()
                except:
                    mainActListen = None
                try:
                    #mainActName = act.find("h3").get_text().replace("— Reinhören", "").replace("  ","")
                    act.find("h3").find("em").extract()
                    mainActName = act.find("h3").get_text().replace("— Reinhören", "").replace("  ","").replace("Reinhören", "")
                except:
                    mainActName = None
                try:
                    mainActWebsite = act.find("h3").find("a")["href"]
                except:
                    mainActWebsite = None
                
                mainActs.append({"description": mainActDescription,
                "genre": mainActGenre,
                "name": mainActName,
                "website": mainActWebsite,
                "image": mainActImage,
                "listen": mainActListen}) # Add listen url later

        supportActs = []
        try:
            supports = soup.find("div", {"class":"support"}).findAll("li", {"class": "support-act"})
            for support in supports:

                try:
                    support.find("em").extract()
                except:
                    pass
                try:
                    supportGenre = support.find("small", {"class": "category"}).get_text()
                    support.find("small", {"class": "category"}).extract()
                except:
                    pass
                try:
                    supportListen = support.find("small", {"class": "listen"}).find("a")["href"]
                    support.find("small", {"class": "listen"}).extract()
                except:
                    supportListen = None
                supportName = support.find("strong").get_text().replace("  ", "")
                try:
                    supportWebsite = support.find("strong").find("a")["href"]
                except:
                    supportWebsite = None
                #supportWebsite = supportName = support.find("strong").get_text()

                #print(support)
                supportActs.append({
                    "name": supportName,
                    "website": supportWebsite,
                    "genre": supportGenre,
                    "listen": supportListen
                })
        except:
            #print("Error decoding support act.")
            supportActs = []



        entryJson = {"eventTitle" : eventHeader,
        "eventDate": date.strip(),
        "eventDay": date_weekday,
        "eventDescription": eventDescription,
        "mainActs": mainActs,
        "isSpecial": isSpecial,
        "isGoes": isGoes,
        "isNew": isNew,
        "isSoldOut": isSoldOut,
        "isTipp": isTipp,
        "isExclusive": isExclusive,
        "isFree": isFree,
        "freeMember": freeMember,
        "isCancelled": isCancelled,
        "isMoved": isMoved,
        "till0530": till0530,
        "isSpecial": isSpecial,
        "plus18":plus18,
        "plus20": plus20,
        "plus21": plus21,
        "plus25": plus25,
        "plus30": plus30,
        "images": eventImg,
        # TO IMPLEMENT
        "ticket": None,
        "doors": doors[0],
        "supportActs": supportActs,
        "facebookEvent": facebookEvent } #put it in JSON
        jsonData['events'].append(entryJson)
        eventImg = None
    return jsonData

#print(processLinks(getLinks()))
#processLinks(getLinks())
with open("./events.json", "w", encoding="utf-8") as writeJSON:
    json.dump(processLinks(getLinks()), writeJSON, indent=4, sort_keys=True)
writeJSON.close()
